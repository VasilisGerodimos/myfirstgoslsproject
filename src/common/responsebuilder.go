package responsebuilder

import (
	"bytes"
	"encoding/json"

	"github.com/aws/aws-lambda-go/events"
)

// Response aasskk
type Response events.APIGatewayProxyResponse

// ResponseBody eeoo
type responseBody struct {
	Error      string `json:"error"`
	Message    string `json:"message"`
	MessageKey string `json:"messageKey"`
	// messageKeyValues string `json:"messageKeyValues"`
}

// BuildResponse aasskk
func BuildResponse(code int, err string, message string, messageKey string) *Response {
	var buf bytes.Buffer

	resBody := &responseBody{
		Error:      err,
		Message:    message,
		MessageKey: messageKey,
	}

	body, _ := json.Marshal(resBody)

	json.HTMLEscape(&buf, body)

	p := Response{StatusCode: code}
	p.Headers = map[string]string{
		"Access-Control-Allow-Origin":      "*",
		"Access-Control-Allow-Credentials": "true",
	}
	p.Body = buf.String()
	return &p
}

// BadRequest Function
func BadRequest(messageKey ...string) *Response {
	if len(messageKey) == 0 {
		return BuildResponse(400, "Bad Request", "Invalid query", "")
	}
	return BuildResponse(400, "Bad Request", "Invalid query", messageKey[0])
}

// Forbidden function
func Forbidden(messageKey ...string) *Response {
	if len(messageKey) == 0 {
		return BuildResponse(403, "Forbidden", "Access forbidden", "")
	}
	return BuildResponse(403, "Forbidden", "Access forbidden", messageKey[0])
}

// Unauthorized function
func Unauthorized(messageKey ...string) *Response {
	if len(messageKey) == 0 {
		return BuildResponse(401, "Unauthorized", "Access forbidden", "")
	}
	return BuildResponse(401, "Unauthorized", "Access forbidden", messageKey[0])
}

// NotFound function
func NotFound(messageKey ...string) *Response {
	if len(messageKey) == 0 {
		return BuildResponse(404, "Not Found", "Resource not found", "")
	}
	return BuildResponse(404, "Not Found", "Resource not found", messageKey[0])
}

// InternalServerError function
func InternalServerError(messageKey ...string) *Response {
	if len(messageKey) == 0 {
		return BuildResponse(500, "Internal Server Error", "An internal server error occurred", "djdj")
	}
	return BuildResponse(500, "Internal Server Error", "An internal server error occurred", messageKey[0])
}
