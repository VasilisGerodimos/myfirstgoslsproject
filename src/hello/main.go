package main

import (
	"context"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	responsebuilder "gitlab.com/VasilisGerodimos/myfirstgoslsproject/src/common"
)

// Response is of type APIGatewayProxyResponse since we're leveraging the
// AWS Lambda Proxy Request functionality (default behavior)
//
// https://serverless.com/framework/docs/providers/aws/events/apigateway/#lambda-proxy-integration
type Response events.APIGatewayProxyResponse

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(ctx context.Context) (responsebuilder.Response, error) {
	resp := *responsebuilder.BadRequest()
	return resp, nil
}

func main() {
	lambda.Start(Handler)
}
